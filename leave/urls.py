from django.conf.urls import url, include
from .import views
from django.contrib import admin
from django.conf import settings
from django.conf.urls.static import static


urlpatterns = [

    url(r'^authentication_error/', views.ErrorView.as_view(), name="auth_error"),
    url(r'^logout/',views.logout_view,name="logout"),
    url(r'^leave-submit/',views.LeaveSubmit,name="submitleave"),
    url(r'^leave-applications/',views.Appliedleave,name="leave-requests"),
    url(r'^employee-database/',views.EmpDatabase,name="employee-database"),
    url(r'^leave-requests/',views.AllLeave,name="all-leaves"),
    url(r'^change-status/', views.StatusChange, name="change-status"),
    url(r'^change-status-admin/', views.AdminstatChange, name="change-status-admin"),
    url(r'^leave-history/',views.LeaveHistory,name="leave-history"),
    url(r'^user-approval/',views.UserApproval,name="user-approval"),
    url(r'^approve-user/',views.ApproveUser,name="approve-user"),
    url(r'^holiday-list/',views.HolidayList.as_view(),name="holiday-list"),
    url(r'^cancel/(?P<pk>\d+)/$',views.CancelReq,name="cancel_leave"),

]



