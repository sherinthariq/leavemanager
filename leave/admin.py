from django.contrib import admin
from .models import *


# Register your models here.
admin.site.register(LeaveData)
admin.site.register(EmployeeDatabase)
admin.site.register(PermisionProfile)