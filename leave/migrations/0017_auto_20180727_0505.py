# Generated by Django 2.0.7 on 2018-07-27 05:05

from django.db import migrations, models
import django.db.models.deletion
import leave.models


class Migration(migrations.Migration):

    dependencies = [
        ('leave', '0016_auto_20180727_0456'),
    ]

    operations = [
        migrations.AlterField(
            model_name='employeedatabase',
            name='profile',
            field=models.ForeignKey(default=1, on_delete=django.db.models.deletion.CASCADE,
                                    to='leave.PermisionProfile'),
        ),
    ]
