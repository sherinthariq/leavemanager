# Generated by Django 2.0.7 on 2018-09-01 09:22

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('leave', '0028_employeedatabase_user'),
    ]

    operations = [
        migrations.AlterField(
            model_name='leavedata',
            name='admin_comments',
            field=models.TextField(blank=True, default='No comments', max_length=100, null=True),
        ),
    ]
