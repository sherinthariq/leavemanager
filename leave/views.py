from django.http import HttpResponse
from django.shortcuts import render, redirect

# Create your views here.
from django.shortcuts import render
from django.views.generic import TemplateView
from django.contrib.auth import logout, login
from django.contrib.auth.decorators import login_required
from django.views import View
from .models import *
from datetime import datetime, date
from django.contrib.auth.models import User
from django.contrib.auth import authenticate
from django.contrib import messages
from django.utils.decorators import method_decorator



@login_required
def home(request):
    p =PermisionProfile.objects.get(id=1)
    usr = request.user
    ld_user = LeaveData.objects.filter(employee=request.user)
    ld_all = LeaveData.objects.all()
    if(request.user.is_superuser):
        return render(request,'sample.html')
    else:
        username = usr.username
        email = usr.email
        try:
            employee = EmployeeDatabase.objects.get(email=usr.email)
        except EmployeeDatabase.DoesNotExist:
            logout(request)
            EmployeeDatabase.objects.create(username=username,email=email,approved=False,profile=p)

            return redirect('auth_error')

        if employee.approved == True:
            ep = str(employee.profile)
            print(ep)
            context = {'ep':ep,'ld_user':ld_user,'ld_all':ld_all}
            return render(request, 'home.html',context)
        else:
            logout(request)
            return redirect('auth_error')

def LeaveSubmit(request):

    if request.method == 'POST':
        l_date = request.POST.get('leave-date','')
        l_type = request.POST.get('leave-type', '')
        remarks = request.POST.get('leave-remarks', '')
        employee = request.user
        l_data = LeaveData(type=l_type, remarks=remarks,date=l_date,employee=employee)
        l_data.save()
    return redirect('/')


@login_required
def Appliedleave(request):
    context={}
    applied_leaves = LeaveData.objects.filter(employee=request.user,processed=False)
    all_leaves = LeaveData.objects.all()
    # employee = EmployeeDatabase.objects.get(email=request.user.email)
    employee = EmployeeDatabase.objects.get(email=request.user.email)
    ep = str(employee.profile)
    context = {'applied_leaves':applied_leaves, 'all_leaves':all_leaves ,'ep':ep}

    return render(request,'applied_leave.html',context)

@login_required
def LeaveHistory(request):
    leave_history = LeaveData.objects.filter(employee=request.user)
    employee = EmployeeDatabase.objects.get(email=request.user.email)
    ep = str(employee.profile)
    context={'leave_history':leave_history,'ep':ep}

    return render(request,'leave-history.html',context)

@login_required
def AllLeave(request):
    all_leaves = LeaveData.objects.filter(processed=False).exclude(employee=request.user)
    employee = EmployeeDatabase.objects.get(email=request.user.email)
    ep = str(employee.profile)
    context = {'all_leaves':all_leaves,'ep':ep}

    return render(request,'all_requests.html',context)


def CancelReq(request,pk):
    leave = LeaveData.objects.get(id=pk)
    leave.status = 'Cancelled'
    leave.processed = True
    leave.save()
    return redirect('leave-requests')

@login_required
def EmpDatabase(request):
    emp_data = EmployeeDatabase.objects.all()
    employee = EmployeeDatabase.objects.get(email=request.user.email)
    ep = str(employee.profile)
    context = {'emp_data':emp_data,'employee':employee,'ep':ep}
    return render(request,'emp- database.html',context)


def StatusChange(request):
    if request.method == 'POST':
        status = request.POST.get('status-change','')
        leave_id = request.POST.get('leaveId','')
        tl_remarks = request.POST.get('tl-remarks','')
        admin_remarks = request.POST.get('admin-remarks','')
        leave = LeaveData.objects.get(id=leave_id)
        leave.int_status = status
        leave.tl_comments = tl_remarks
        leave.save()

    return redirect('all-leaves')

def AdminstatChange(request):
    if request.method == 'POST':
        status = request.POST.get('status-change','')
        leave_id = request.POST.get('leaveId','')

        admin_remarks = request.POST.get('admin-remarks','')
        leave = LeaveData.objects.get(id=leave_id)
        leave.status = status
        leave.admin_comments = admin_remarks
        leave.processed = True
        leave.save()

    return redirect('all-leaves')


def UserApproval(request):
    pending_users = EmployeeDatabase.objects.filter(approved=False)
    profile = PermisionProfile.objects.all()

    context = {'pending_users':pending_users,'profile':profile}

    return render(request,'user-approval.html',context)

def ApproveUser(request):
    if request.method == 'POST':
        id = request.POST.get('id','')

        profile = request.POST.get('profile','')

        p = PermisionProfile.objects.get(id=profile)
        employee = EmployeeDatabase.objects.get(id=id)
        employee.approved = True
        employee.profile = p
        employee.save()

    return redirect('user-approval')



class ErrorView(TemplateView):
    template_name = "auth_error.html"


def logout_view(request):
    logout(request)

    return redirect('/')

@method_decorator(login_required, name='dispatch')
class HolidayList(TemplateView):
    template_name = 'holiday_list.html'


    def get_context_data(self, **kwargs):
        employee = EmployeeDatabase.objects.get(email=self.request.user.email)
        ep = str(employee.profile)
        context = {'ep': ep}
        return  context

