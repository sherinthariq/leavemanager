from django.db import models
from django.contrib.auth.models import User
import datetime


class LeaveData(models.Model):

    status_choices= (
        ('Under review', 'Under review'),
        ('Approved', 'Approved'),
        ('Rejected', 'Rejected'),
        ('Cancelled','Cancelled'),
    )

    type_choices = (
        ('Casual leave' , 'Casual leave'),
        ('Sick leave' , 'Sick leave'),
        ('Work from home','Work from home'),
    )

    type = models.CharField(choices=type_choices,max_length=30,blank=False,null=False)
    date = models.DateField(blank=False, null=False, default=datetime.date.today)
    status = models.CharField(max_length=15, choices=status_choices,default='Under review')
    remarks = models.TextField(blank=False,null=False)
    employee =models.ForeignKey(User,on_delete=models.CASCADE,blank=False,null=False)
    processed = models.BooleanField(default=False,blank=True)
    int_status = models.CharField(max_length=15, choices=status_choices,blank=True,null=True,default='Under Review')
    tl_comments = models.TextField(max_length=100,blank=True)
    admin_comments = models.TextField(max_length=100, blank=True,null=True,default='No admin comments')
    cancel_request = models.BooleanField(default=False,blank=True)

    def __str__(self):
        return self.employee.username


class PermisionProfile(models.Model):
    p_choices = (
        ('Super Admin','Super Admin'),
        ('Admin', 'Admin'),
        ('Normal user', 'Normal user'),
        ('Team lead', 'Team lead'),

    )
    profile_choices =  models.CharField(max_length=30,choices=p_choices,default='Normal user')


    def __str__(self):
        return self.profile_choices




class EmployeeDatabase(models.Model):

    user = models.ForeignKey(User,blank=True,null=True,on_delete=models.CASCADE)
    username = models.CharField(max_length=30)
    email = models.EmailField(max_length=25)
    approved = models.BooleanField(default=False)
    profile = models.ForeignKey(PermisionProfile,on_delete=models.CASCADE)


    def __str__(self):
        return self.email


